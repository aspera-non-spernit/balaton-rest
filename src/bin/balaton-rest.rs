#![feature(proc_macro_hygiene, decl_macro)]
extern crate balaton_models;
#[macro_use] extern crate rocket;
#[macro_use] extern crate serde;
use balaton_models::{io::BalatonRequest, properties::Property };
use rocket::{ form::{ Form, FromForm }, serde::{ Deserialize, json::{ Json, Value } }, State };
use std::{ fs, path::PathBuf };

/// curl -X POST http://127.0.0.1:8001/api --data '{ "path" : "properties", "query": "all" }' -H "Content-Type: application/json"
#[post("/api", format = "application/json", data = "<request>")]
async fn api<'r>(request: Json<BalatonRequest<'r>>) -> Value {
    match request.path {
        "properties" =>  { data_source(DataType::Properties).unwrap() },
        _ => {
            serde_json::from_str( r#"{ "error" : "path not available", "links" : ["properties"]}"# ).unwrap()
        }
    }
}

fn data_source(data_type: DataType) -> Result<Value, Box<dyn std::error::Error> > {
    let data_str = match data_type {
        DataType::Properties => { fs::read_to_string("data/properties.json")? }
    };
    let v: Value = serde_json::from_str(&data_str)?;
    Ok(v)
}

enum DataType {
    Properties
}
#[launch]
fn rocket() -> _ {
    rocket::build()
        .mount("/", routes![api]) 
}